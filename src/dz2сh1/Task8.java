package dz2сh1;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = console.nextInt();
        }
        int m = console.nextInt();
        int min = arr[0];
        int abcMin = Math.abs(arr[0] - m);
        for (int j = 1; j < arr.length; j++) {
            int temp = Math.abs(arr[j] - m);
            if(temp <= abcMin) {
                abcMin = temp;
                min = arr[j];
            }
        }
        System.out.println(min);
    }
}

