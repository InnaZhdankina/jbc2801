package dz2сh1;

import java.util.Scanner;

public class Task51 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = console.nextInt();
        }

        int m = console.nextInt();

        int[] arr1 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if ((i + m) < arr.length) {
                arr1[i + m] = arr[i];
            } else {
                arr1[(i + m) - arr.length] = arr[i];
            }

        }
        for (int i = 0; i < arr1.length; i++)
            System.out.print(arr1[i] + " ");

    }
}

