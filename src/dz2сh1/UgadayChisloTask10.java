package dz2сh1;
import java.util.Random;

import java.util.Scanner;
public class UgadayChisloTask10 {
    public static void main(String[] args) {
        runGame();
    }
    public static void runGame() {
        Random r = new Random();
        int number = r.nextInt(1001);
        Scanner console = new Scanner(System.in);
        while (true) {
            System.out.println("Угадайте число");
            System.out.println("Введите число, которое вы угадали");
            int input = console.nextInt();
            if (input > number) {
                System.out.println("Это число больше загаданного.");
            } else if (input < number && input >= 0) {
                System.out.println("Это число меньше загаданного.");
            } else if (input == number) {
                System.out.println("Победа!");
                break;
            } else if (input < 0) {
                break;
            }
        }
    }
}
