package dz2сh1;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int k = 0; k < arr.length; k++) {
            arr[k] = sc.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            int currentElementCount = 1;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] != arr[j]) {
                    break;
                }
                currentElementCount++;
            }
            System.out.println(currentElementCount + " " +arr[i]);
            i += currentElementCount - 1;
        }
    }
}

