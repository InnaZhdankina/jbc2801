package dz2сh1;

import java.util.Scanner;

public class Task61 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String strCode = console.next();
        System.out.println(MorzeCode(strCode.toUpperCase()));
    }
    private static String MorzeCode(String strCode) {
        String str = "";
        String [] morzeCode = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..",
                "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...",
                "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--",
                "-..-", "..-..", "..--", ".-.-"};

        for (int i = 0; i < strCode.length(); i++) {
            str += morzeCode[strCode.charAt(i) - 1040] + " ";
        }

        return  str.trim();
    }
}

