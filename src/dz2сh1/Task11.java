package dz2сh1;
import java.security.SecureRandom;
import java.util.Scanner;

public class Task11 {
    public static String generateRandomPassword(int len) {

        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_*-";

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();

        // каждая итерация цикла случайным образом выбирает символ из заданного
        // диапазон ASCII и добавляет его к экземпляру `StringBuilder`

        for (int i = 0; i < len; i++) {
            int randomIndex = random.nextInt(chars.length());
            sb.append(chars.charAt(randomIndex));
        }

        return sb.toString();
    }

    public static void main(String[] args) {

        Scanner input  = new Scanner(System.in);
        while (true) {
            System.out.println("Введите число - длина желаемого пароля");
            int len = input.nextInt();
            if (len >= 8) {
                System.out.print("Пароль: ");
                System.out.println(generateRandomPassword(len));
                break;
            } else {
                System.out.println("Пароль с" + " " + len + " " + "количеством символов небезопасен");
            }
        }
    }
}
//Test


